//creo una clase padre
class padre{
    //atributos
    nombre='Gerson Perez';
    edad='47';
    //metodos
    trabajar(){
        console.log("trabajando...");
    }
    cocinar(){
        console.log("cocinando...");
    }
    OficiosDeLaCasa(){
        console.log("haciendo oficios de la casa...");
    }
}
//creo las clases madre, hijo e hija, como herencia de la clase padre
class madre extends padre{
    //atributos
    nombre='Mireya Espinosa';
    edad='45';
    //metodos
}
class hijo extends padre{
    //atributos
    nombre='Nicolas Perez Espinosa';
    edad='18';
    //metodos
    trabajar(){
        console.log("trabajo ayudando a mis padres");
    }
    estudiar(){
        console.log("estudio ingenieria catrastral y geodesia");
    }
}
class hija extends padre{
    //atributos
    nombre='Mariana Perez Espinosa';
    edad='21';
    //metodos
    estudiar(){
        console.log("estudio ingenieria quimica");
    }
}
let mipadre=new padre();
let mimadre=new madre();
let yo=new hijo();
let mihermana=new hija();